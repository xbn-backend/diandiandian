<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include_once ("includes/head.php"); ?> 
        <?php include_once ("includes/cssjs.php"); ?> 
        <title>殺價王</title>
        <script>
            $(function(){
                    setTimeout(function() {
                        $(".letter").addClass("loaded");
                        $(".reg-text").addClass("loaded");
                    }, 1000);
            });
        </script>
    </head>

    <body>
        <?php include_once("includes/header.php"); ?>
        <div id="shajia">
            <div class="page-content d-flex align-items-stretch">
                <div class="headerbanner">
                    <div class="text-container">
                        <span class="reg-text">我們</span>
                        <span class="letter">是</span>
                        <span class="letter">誰</span>
                        <span class="letter">?</span>
                    </div>
                    <div class="text-container-2">
                        <span class="reg-text">我們</span>
                        <span class="letter">要</span>
                        <span class="letter">做</span>
                        <span class="letter">什</span>
                        <span class="letter">麼</span>
                        <span class="letter">?</span>
                    </div>
                    <div class="text-container-3">
                        <span class="reg-text">我們</span>
                        <span class="letter">為</span>
                        <span class="letter">您</span>
                        <span class="letter">帶來</span>
                        <span class="letter">什麼</span>
                        <span  class="letter">?</span>
                    </div>
                    <div class="text-container-4">
                        <span class="reg-text">我們</span>
                        <span class="letter">需</span>
                        <span class="letter">要</span>
                        <span class="letter">什麼</span>
                        <span class="letter">?</span>
                    </div>
					<div class="text-container-5">
						<!--div class="content animated fadeInUp"-->
							<div class="dian-download-box">
								<a class="btn btn-lg dian-download" style="color:#FFFFFF;" href="https://play.google.com/store/apps/details?id=tw.com.saja.userapp" target=_blank>
									<i class="fa fa-android" aria-hidden="true"></i> Android <i class="fa fa-angle-down" aria-hidden="true"></i>
								</a>
								<a class="btn btn-lg dian-download" style="color:#FFFFFF;" href="https://itunes.apple.com/tw/app/%E5%8F%B0%E7%81%A3%E6%AE%BA%E5%83%B9%E7%8E%8B/id1441402090?mt=8" target=_blank>
									<i class="fa fa-apple" aria-hidden="true"></i> iOS <i class="fa fa-angle-down" aria-hidden="true"></i>
								</a>
							</div>
						<!--/div-->	
					</div>
                </div>
			
				
            </div>
            <div class="main">
                <div id="first" class="main-item">
                    <h2 class="title">我們是誰？</h2>
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                                <img class="img-fluid" src="static/images/sha1-04.png" alt="">
                            </div>
                            <div class="col-md">
                                <div class="p-box mt-5 mt-md-0">
                                    <p>殺價王是一個創新型的行銷導購平臺，擁有三項發明專利：</p>
                                    <p>美國發明專利 Method and system for an auction based on reducing BID 證號：7853484</p>
                                    <p>臺灣發明專利「殺價式拍賣之方法」證號：I305629</p>
                                    <p>中國專利「殺價式拍賣系統」證號：ZL201120006296</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="title">全球首創 「殺價式」 購物平台</h2>
                    <div class="article-box">
                        <img class="img-fluid" src="static/images/sha1-02.png" alt="">
                        <div class="p-box">
                            <h3>最低且價格唯一</h3>
                            <p>全新商品,最低起價，</p>
                            <p>規則為使用殺價幣標售商品，出價"最低"且"唯一"者得標</p>
                            <p>臺灣以1元為單位，中國大陸以0.01元(1分)人民幣為單位</p>
                            <p>其餘沒有得標者所付之殺價幣將全數 1：1 反為鯊魚點</p>
                            <p>擁有鯊魚點可以隨意的到所有合作廠商使用。</p>
                        </div>
                    </div>
                    <div class="article-box">
                        <img class="img-fluid" src="static/images/sha1-03.png" alt="">
                        <div class="p-box">
                            <h3>最低且價格唯一</h3>
                            <p>全新商品,最低起價，</p>
                            <p>規則為使用殺價幣標售商品，出價"最低"且"唯一"者得標</p>
                            <p>臺灣以1元為單位，中國大陸以0.01元(1分)人民幣為單位</p>
                            <p>其餘沒有得標者所付之殺價幣將全數 1：1 反為鯊魚點</p>
                            <p>擁有鯊魚點可以隨意的到所有合作廠商使用。</p>
                        </div>
                    </div>
                </div>
                <div class="main-item">
                    <h2 class="title">合作目標</h2>
                    <div class="article-box">
                        <img class="img-fluid" src="static/images/sha1-06.png" alt="">
                    </div>
                </div>
                <div class="main-item">
                    <h2 class="title">鯊魚點已導購商家與服務</h2>
                    <div class="article-box">
                        <img class="img-fluid" src="static/images/about2-01.png" alt="">
                    </div>
                    <h2 class="title">鯊魚點已導購商家與服務(中國大陸)</h2>
                    <div class="article-box">
                        <img class="img-fluid" src="static/images/sha1-05.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <?php include_once("includes/footer.php"); ?>
    </body>
</html>