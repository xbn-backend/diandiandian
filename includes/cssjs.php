<!-- cssjs -->
<!-- ICON -->
<link rel="shortcut icon" href="static/favicon.ico" type="image/x-icon" />
<link rel="Bookmark" href="static/favicon.ico" type="image/x-icon" />
<!-- 瀏覽器設定重置 -->
<link rel="stylesheet" href="static/css/reset.css">
<!-- Bootstrap -->
<link rel="stylesheet" href="static/vendor/bootstrap/4.0.0/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"/>
<link rel="stylesheet" href="static/css/animate.css">

<link rel="stylesheet" href="static/css/index-header-text.css">
<!-- font-awesome CDN -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- 自定義CSS -->
<link rel="stylesheet" href="static/css/all.css">
<!-- 自定義RWD -->
<link rel="stylesheet" href="static/css/responsive.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.1.1/masonry.pkgd.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

<script src="static/vendor/bootstrap/4.0.0/bootstrap.bundle.min.js"></script>

<script>
    //header往下滑後 改變顏色
    //$("#first").offset().top)-1
    
    function changeColor(){
        if($(".navbar-toggler").hasClass('collapsed')){
            $(".dian-topnav").removeClass("white").addClass("black");
        }else{
            $(".dian-topnav").removeClass("black").addClass("white");
        }
    }
    
    $(function () {
        $("#gotop").hide(); //隱藏按鈕
        $(window).scroll(function(){
            if($(window).scrollTop()<200){
                if($(".navbar-toggler").hasClass('collapsed')){
                    $(".dian-topnav").removeClass("black").addClass("white");
                }else{
                    $(".dian-topnav").removeClass("white").addClass("black");
                }
                $("#gotop").fadeOut(700);
            }else{
                $(".dian-topnav").removeClass("white").addClass("black");
                $("#gotop").fadeIn(700);
            }
        });
        $("#gotop").click(function(){
            $('body,html').animate({scrollTop:0},500); //單擊按鈕，捲軸捲動到頂部
            return false;
        })
        
        $(".navbar-toggler").on('click',function(){
            if($(window).scrollTop()<200){
                if($(".navbar-toggler").hasClass('collapsed')){
                    $(".dian-topnav").removeClass("white").addClass("black");
                }else{
                    $(".dian-topnav").removeClass("black").addClass("white");
                }
            }
        })
       
    });
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118000342-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118000342-1');
</script>