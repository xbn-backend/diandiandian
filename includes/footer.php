<footer class="dian-footer">
    <div class="row">
        <div class="col-md footer-box">
            <div class="info-box">
                <span class="title">phone</span>
                <span class="txt">+886 2 2738 0898</span>
            </div>
            <div class="info-box">
                <span class="title">mail</span>
                <span class="txt">gillwu0101@sharebonus.com</span>
            </div>
        </div>
        <div class="col-md footer-box">
            <div class="info-box">
                <span class="title">點點點行銷有限公司</span>
            </div>
            <div class="info-box">
                <span class="txt">110台北市信義區和平東路三段255號7樓</span>
                <span class="txt">7F., No.255, Sec. 3, Heping E. Rd., Xinyi Dist., Taipei City 110, Taiwan (R.O.C.)</span>
            </div>
        </div>
        <div class="col-md footer-box">
            <div class="info-box">
                <span class="title">臺北公司</span>
                <span class="txt">點點點行銷有限公司</span>
            </div>
            <div class="info-box">
                <span class="title">上海公司</span>
                <span class="txt">上海巴根網絡科技有限公司</span>
            </div>
            <div class="info-box">
                <span class="title">苏州公司</span>
                <span class="txt">鲨价王网络科技有限公司</span>
            </div>
        </div>
    </div>

    <!-- <div class="row">
<div class="col-md-4 footer01"><span>phone: +886 2 2738 0898</span><span>mail:gillwu@sharebonus.com
</span></div>
<div class="col-md-4 footer01"><span>phone: +886 2 2738 0898</span><span>mail:gillwu@sharebonus.com
</span></div>
<div class="col-md-4 footer01"><span>phone: +886 2 2738 0898</span><span>mail:gillwu@sharebonus.com
</span></div>
</div> -->

</footer>
<div id="gotop" class="text-center"><i class="fa fa-chevron-up" aria-hidden="true"></i><span>Top</span></div>

