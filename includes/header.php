<!--
<nav class="navbar full-width dropdownmenu">
    <div class="fixed-width">
        <div class="logo"><a href="index.php"><img src="static/images/logo.png" alt=""></a></div>
        <div class="menu">
        <ul>
        <li><a href="index.php" class="white">HOME</a></li>
        <li> <a href="about.html" class="white">ABOUT</a> </li>
        <li><a href="" class="white">項目</a>

        <ul id="submenu">
        <li><a href="sharebonus.php" class="white">雪波拿</a></li>
        <li><a href="shajia.php" class="white">殺價王</a></li>
        </ul>
        </li>

        <li><a href="about.php" class="white">關於我們</a></li>
        <li><a href="contact.php" class="white">CONTACT</a></li>
        </ul>
        </div>
    </div>
</nav>
-->


<nav class="dian-topnav navbar fixed-top navbar-expand-md navbar-dark">
    <a class="navbar-brand" href="index.php">
        <img src="static/images/logo.png" alt="">
    </a>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarContent" data-hover="dropdown" data-animations="fadeInDown fadeInRight fadeInUp fadeInLeft">
        <ul class="navbar-nav ml-auto justify-content-around" data-animations="fadeInUp fadeInLeft fadeInUp fadeInRight">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">首頁</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">產品</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="sharebonus.php">雪波拿</a>
                    <a class="dropdown-item" href="shajia.php">殺價王</a>
                    <a class="dropdown-item" href="gragrago.php">抓抓購</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="about.php">關於我們</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact.php">聯絡我們</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.yourator.co/companies/ShareBonus" target="_blank">加入我們</a>
            </li>
        </ul>
    </div>
</nav>

<!-- 判斷網址在nav添加active -->
<script>
    var _nava = $('.navbar-nav .nav-item a[href!="#"]');
    var _url = window.location.href;
    var _host = window.location.host;
    for(var i = 0; i<_nava.length ; i++){
        var _atext = _nava.eq(i).text();
        var _astr = _nava.eq(i).attr('href');
        if(_url.indexOf(_astr) != -1 ){ //比對網址，相同時添加active
            _nava.eq(i)
                .parents('.nav-item').addClass('active')
                .siblings().removeClass('active');
        }else if(_url == ('http://'+_host+'/')){
            _nava.eq(0)
                .parents('.nav-item').addClass('active')
                .siblings().removeClass('active');
        }
    }
</script>
