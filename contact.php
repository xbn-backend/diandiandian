<!DOCTYPE html>
<html>
    <head>
        <?php include_once ("includes/head.php"); ?> 
        <?php include_once ("includes/cssjs.php"); ?>    
        <title>聯絡我們</title>
        <script>
            /*欄位驗證*/
            $(document).on("ready", function() {
                $('#sendemail').click(function(e){
                    if($("#input-name").val()==""){
                        e.preventDefault();
                        $(".alert")
                            .css("display","block")
                            .text("Name欄位不得為空!!");
                        $("#input-name").focus();
                    }else if($("#input-email").val()==""){
                        e.preventDefault();
                        $(".alert")
                            .css("display","block")
                            .text("Email欄位不得為空!!");
                        $("#input-email").focus();
                    }else if(!$("#input-email").val()==""){
                        var rulel=/^[0-9a-zA-Z]([-._]*[0-9a-zA-Z])*@[0-9a-zA-Z]([-._]*[0-9a-zA-Z])*\.+[a-zA-Z]+$/;
                        if(!rulel.test($("#input-email").val())){
                            e.preventDefault();
                            $(".alert")
                                .css("display","block")
                                .text("Email格式有誤!!");
                            $("#input-email").focus();
                        }
                    }else if($("#input-subject").val()==""){
                        e.preventDefault();
                        $(".alert")
                            .css("display","block")
                            .text("主旨欄位不得為空!!");
                        $("#input-subject").focus();
                    }else if($("#input-message").val()==""){
                        e.preventDefault();
                        $(".alert")
                            .css("display","block")
                            .text("內容欄位不得為空!!");
                        $("#input-message").focus();
                    }else{
                        $(".alert")
                            .css("display","none")
                            .text("");
                        $.ajax({
                            url:"mailer.php",
                            data:{
                                subject:$('#input-subject').val(),
                                name:$('#input-name').val(),
                                email:$('#input-email').val(),
                                message:$('#input-message').val(),
                                sendto:"gillwu0101@sharebonus.com"
                            },
                            dataType:"text",
                            success: function(ret) {
                                console.log((typeof ret));
                                json=ret;
                                if("string"==(typeof ret)) {
                                    json=JSON.parse(ret);
                                } 
                                alert(json.retmsg);
                            }
                        });
                    }
                });
                $("#input-name").blur(function(){
                    if($(this).val()==''){
                        $(".alert")
                            .css("display","block")
                            .text("Name欄位必填!!");
                    }else{
                        $(".alert")
                            .css("display","none")
                            .text("");
                    }
                });
                $("#input-email").blur(function(){
                    var rulel=/^[0-9a-zA-Z]([-._]*[0-9a-zA-Z])*@[0-9a-zA-Z]([-._]*[0-9a-zA-Z])*\.+[a-zA-Z]+$/;
                    if($(this).val()==''){
                        $(".alert")
                            .css("display","block")
                            .text("Email欄位必填!!");
                    }else if(!rulel.test($("#input-email").val())){
                        $(".alert")
                                .css("display","block")
                                .text("Email格式有誤!!");
                    }else{
                        $(".alert")
                            .css("display","none")
                            .text("");
                    }
                });
                $("#input-subject").blur(function(){
                    if($(this).val()==''){
                        $(".alert")
                            .css("display","block")
                            .text("主旨欄位必填!!");
                    }else{
                        $(".alert")
                            .css("display","none")
                            .text("");
                    }
                });
                $("#input-message").blur(function(){
                    if($(this).val()==''){
                        $(".alert")
                            .css("display","block")
                            .text("內容欄位必填!!");
                    }else{
                        $(".alert")
                            .css("display","none")
                            .text("");
                    }
                });
            });
        </script>
    </head>
    <body>
        <?php include_once("includes/header.php"); ?>
        <div id="contact">
            <div class="page-content d-flex align-items-stretch">
                <div class="headerbanner">
                    <div class="container">
                        <form class="cf" Action="mailer.php" id="contact_form">
                            <h1>聯絡我們</h1>
                            <div class="form-group">
                                <input class="form-control form-control-lg" type="text" name="name" id="input-name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input class="form-control form-control-lg" type="email" name="email" id="input-email" placeholder="Email address">
                            </div>
                            <div class="form-group">
                                <input class="form-control form-control-lg" type="text" name="subject" id="input-subject" placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control form-control-lg" rows="4" name="message" type="text" id="input-message"></textarea>
                            </div>
                            <button class="btn btn-block" type="submit" id="sendemail" name="sendemail">傳送</button>
                        </form>
                        <div class="alert alert-danger" role="alert" style="display:none;">請填寫完整內容</div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once("includes/footer.php"); ?>
    </body>
</html>