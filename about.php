<!DOCTYPE html>
<html>
    <head>
        <?php include_once ("includes/head.php"); ?> 
        <?php include_once ("includes/cssjs.php"); ?> 
        <title>關於我們</title>

<!--
        <script>
            $(document).ready(function(){
                var a = $("#about").offset().top;
                var b = $("#section-a").offset().top;
                var c = $("#section-c").offset().top;
                var d = $("#section-d").offset().top;
                console.log(d);
                $(".aboutus").click(function(){
                    $("html,body").animate({
                        scrollTop:a
                    });
                });
                $("nav a:nth-of-type(2)").click(function(){
                    $("html,body").animate({
                        scrollTop:b
                    });
                });
                $("nav a:nth-of-type(3)").click(function(){
                    $("html,body").animate({
                        scrollTop:c
                    });
                });
                $("nav a:nth-of-type(4)").click(function(){
                    $("html,body").animate({
                        scrollTop:d
                    });
                });

            })

        </script>
-->
    </head>
    <body>
        <?php include_once("includes/header.php"); ?>
        <div id="dian-about">
            <div class="img-box">
                <img class="img-fluid" src="static/images/our.png" alt="">
                
                <!-- 尺寸768以上顯示 -->
                <div class="dian-slogan animated fadeInUp d-none d-md-block">
                    <h1>seek constant professional renewal and growth</h1>
<!--
                    <p>sdsd sdgsgs xcdsfse</p>
                    <a href="javascript:void(0);" class="aboutus">MORE</a>
-->
                </div>
                
            </div>
            
            <div class="main">
                <div id="first" class="main-item">
                    <h2 class="title">關於我們</h2>
                    
                    <!-- 尺寸768以下顯示 -->
                    <div class="dian-slogan-2 d-block d-md-none">
                        <div class="article-box">
                            <h3>seek constant professional renewal and growth</h3>
                        </div>
                    </div>
                    
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-12 col-md-5">
                                <img class="img-fluid about-img1" src="static/images/us01.png" alt="">
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="p-box mt-5 mt-md-0">
                                    <p>「點點點」在中文的標點符號中，三個點有著待續未完、無限的意義，點點點行銷有限公司就是這樣一個擁有無限創意點子的團隊，我們擅長全新行銷模式發想與社群經營互動，年輕、活力、勇往直前是我們最大的特色。</p>
                                    <p>創始人楊濟成先生與郭文蕙小姐先前共同創辦多家互聯網公司，長期在IT互聯網行業創業，專注在遊戲、視頻、支付領域，也分別經營視頻直播、視屏點播（VOD）、及視訊會議業務，具有豐富的軟體發展及互聯網創業經驗，善於技術研發，並將業務模式商業化，有極強的技術前瞻性，並擁有臺灣、美國、大陸多個發明專利，數次獲得創新創業獎勵，CCTV等眾多媒體對其進行過專訪。</p>
                                    <p>點點點行銷有限公司目前有兩個項目，一個為殺價王，一個為雪波拿，殺價王以獨特的殺價模式的互聯網購物平台，曾經創造了18天開始獲利，31天盈虧平衡的傳奇業績，引起媒體極大關注。</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-item">
                    <h2 class="title">團隊介紹</h2>
                    <div class="container">
                        <div class="row no-gutters">
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="services-img rounded-circle">
                                            <img class="img-fluid" src="static/images/ourteam/ourteam-Joanne.jpg" alt="">
                                        </div>
                                        <div class="services-content">
                                            <h3 class="name">郭文蕙<small>(Joanne Kuo)</small></h3>
                                            <h5 class="job">公司聯合創始人</h5>
                                            <p class="intro">行動商務碩士專業畢業，長期在IT互聯網⾏業創業，專注在遊戲、視頻、支付領域，具有豐富的軟體發展及互聯網創業經驗，善於技術研發，並將點子模式商業化，有極強的技術前瞻性。曾開發累計超過50個網站平臺，涵蓋遊戲、支付、旅遊、電子商務、社群交友、線上視頻以及網路直播等。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="services-img rounded-circle">
                                            <img class="img-fluid" src="static/images/ourteam/ourteam-Jeff.jpg" alt="">
                                        </div>
                                        <div class="services-content">
                                            <h3 class="name">楊濟成<small>(Jeff Yang)</small></h3>
                                            <h5 class="job">公司創始人及首席執行官（CEO）</h5>
                                            <p class="intro">善於發明創造，深刻理解消費者，有很強的市場敏銳度及跨界整合能力，在遊戲、視頻、支付領域積累了豐富的互聯網運營、軟體發展及創業經驗。2001年，楊濟成先生加入臺灣知名線上遊戲公司因思銳遊戲總局，首創臺灣線上遊戲虛擬道具購買的商業模式，被廣泛採用。次年，他超越當時頻寬及網路技術局限，開發了跨兩岸的視頻直播系統，並研發了信用卡防盜刷系統。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
<!--
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="services-img rounded-circle">
                                            <img class="img-fluid" src="static/images/ourteam/ourteam-Richard.jpg" alt="">
                                        </div>
                                        <div class="services-content">
                                            <h3 class="name">楊洋<small>(Richard Yang)</small></h3>
                                            <h5 class="job">中國殺價王首席運營官（COO）</h5>
                                            <p class="intro">曾供職于吉利汽車與TCL集團，擁有豐富的市場開拓、運營及銷售管理經驗和國際化業務拓展經歷，將積極推動公司在中國大陸的市場運營，同時拓展公司東南亞的業務佈局。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
-->
                            <div class="col-12 col-sm-6 col-md-4" style="display:none;">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="services-img rounded-circle">
                                            <img class="img-fluid" src="static/images/ourteam/ourteam-sample.png" alt="">
                                        </div>
                                        <div class="services-content">
                                            <h3 class="name">熊玉玲<small>(Bell Bear)</small></h3>
                                            <h5 class="job">中國殺價王首席資料分析師</h5>
                                            <p class="intro">熊玉玲女士擁有10年的軟體發展經驗，4年系統架構師經驗，5年以上項目管理及團隊建設經驗，精通資料採擷、機器學習、資料視覺化、雲計算、企業級ERP、及垂直搜索。曾加入微軟與上海市政府的合資企業上海微創及為野村證券、東京銀行及COSMOS開發企業級ERP系統，參與NEC中國研究院雲計算平臺研發，後在百度地圖、百度金融及百度資訊流從事大資料技術研究工作。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="services-img rounded-circle">
                                            <img class="img-fluid" src="static/images/ourteam/ourteam-Chung.jpg" alt="">
                                        </div>
                                        <div class="services-content">
                                            <h3 class="name">陳中<small>(Chung Chen)</small></h3>
                                            <h5 class="job">中國殺價王公司戰略投資副總裁</h5>
                                            <p class="intro">深諳專案開發、風險評估、投資佈局、執行管理、及資源整合。陳中先生在臺灣海軍服役近20年，曾任多個要職，數次獲得艦隊戰力競賽優秀楷模獎，後出任於臺灣上市公司源大環能股份有限公司副總經理，主管所有業務及公司行政管理，擁有著極強的技術研究和組織管理能力，在殺價王的專案開發、風險評估、執行管理、投資佈局、資源分析及整合等各方面均有顯著貢獻。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="services-img rounded-circle">
                                            <img class="img-fluid img-top-25" src="static/images/ourteam/ourteam-Thomes.jpg" alt="">
                                        </div>
                                        <div class="services-content">
                                            <h3 class="name">劉冠秀<small>(Thomes Liu)</small></h3>
                                            <h5 class="job">工程部經理</h5>
                                            <p class="intro">台灣大學土木工程學研究所畢業，曾任資迅人網路集團/Web RD工程師、數位聯合電信副工程師、精英電腦/資訊部/MIS工程師、華碩電腦/電腦中心/工程師、和碩聯合科技/電腦中心主任。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="services-img rounded-circle">
                                            <img class="img-fluid img-top-15" src="static/images/ourteam/ourteam-Joe.jpg" alt="">
                                        </div>
                                        <div class="services-content">
                                            <h3 class="name">林鈺翔</h3>
                                            <h5 class="job">資深工程師</h5>
                                            <p class="intro">中華技術學院資訊管理系畢業，曾任綠界科技工程師、緯德科技股份有限公司工程師、網路家庭國際資訊股份有限公司資訊工程師、奇寶網路專案部主任。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="services-img rounded-circle">
                                            <img class="img-fluid" src="static/images/ourteam/ourteam-Lu.jpg" alt="">
                                        </div>
                                        <div class="services-content">
                                            <h3 class="name">盧彥誠</h3>
                                            <h5 class="job">資深工程師</h5>
                                            <p class="intro">美國新墨西哥州立大學/電機工程系，曾任中華民國駕駛教育學會/軟體專案工程師、通運駕訓班/班主任特助、IN &OUT COMPUTER/電腦維修工程師、學員線上影音教學系統、駕訓班學員監理管理系統程式編寫-交通部三代系統上線與中華電信配合撰寫駕訓班管理系統程式與使用手冊目前全台40間駕訓班使用中，擁有豐富的程式開發經驗。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="services-img rounded-circle">
                                            <img class="img-fluid" src="static/images/ourteam/ourteam-Gill.jpg" alt="">
                                        </div>
                                        <div class="services-content">
                                            <h3 class="name">吳曉緹</h3>
                                            <h5 class="job">行銷部經理</h5>
                                            <p class="intro">負責龍之最傳媒承接台北市政府客家活動嘉年華記者會及活動專案執行並結案，協助尹星知識管理學院處理對外合作業務及廣告置入，曾任龍之最傳媒有限公司廣告業務助理及尹星知識管理學院執行特助，在專案開發、風險評估、執行管理整合等各方面均有顯著貢獻。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="services-img rounded-circle">
                                            <img class="img-fluid" src="static/images/ourteam/ourteam-Almee.png" alt="">
                                        </div>
                                        <div class="services-content">
                                            <h3 class="name">翁蓓茹</h3>
                                            <h5 class="job">視覺設計總監</h5>
                                            <p class="intro">曾任玩點子工作室視覺設計師、Ladies流行工作坊產品設計，有豐富的UI/UX設計、產品包裝設計、ＤＭ、海報卡片、插畫繪本設計、網頁介面前端設計等多元設計經驗，擁有極大的創意思考力。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once("includes/footer.php"); ?>
    </body>

</html>