<!DOCTYPE html>
<html>
    <head>
        <?php include_once ("includes/head.php"); ?> 
        <?php include_once ("includes/cssjs.php"); ?> 
        <title>點點點行銷</title>
    </head>
    <body>
        <?php include_once ("includes/header.php"); ?>
        <div id="home">
            <div class="page-content d-flex align-items-stretch">
                <div class="headerbanner">
                    <div class="content animated fadeInUp">
                        <h1>點點點</h1>
                        <p>「點點點」在中文的標點符號中，三個點有著待續未完、無限的意義</p>
                        <p>點點點行銷有限公司就是這樣一個擁有無限創意點子的團隊</p>
                        <p>我們擅長全新行銷模式發想與社群經營互動，專營互聯網項目，</p>
                        <p>結合行銷打造最具前瞻性的、及話題性的APP並結合線下引爆各式各樣的新鮮話題</p>
                        <p>年輕、活力、勇往直前使我們極富創造力！</p>
                        <!-- <a  href="javascript:;" class="aboutus">MORE</a> -->
                    </div>
                </div>
            </div>
        </div>
        <?php include_once ("includes/footer.php"); ?>
    </body>
</html>