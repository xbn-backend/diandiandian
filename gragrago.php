<!DOCTYPE html>
<html>
    <head>
        <?php include_once ("includes/head.php");  ?> 
        <?php include_once ("includes/cssjs.php");  ?>
        <title>抓抓購</title>
    </head>
    <body>
        <?php include_once("includes/header.php"); ?>
        <div id="gragrago">
            <div class="page-content d-flex align-items-stretch">
                <div class="headerbanner">
                    <div class="hand">
                        <img src="static/images/gragrago-hand.png" alt="">
                    </div>
                    <div class="content animated fadeInUp">
                        <img class="logo" src="static/images/gragragoLogo.png" alt="">
<!--
                        <h1>雪波拿紅利集點神器</h1>
                        <p>所有民生必需的食衣住行育樂全方位的生活盡在雪波拿</p>
                        <p>獲得的點數隨處、隨時、隨地可用，讓你換到更大的優惠</p>
                        <p>點數不只能自己收集，還可以賺別人的!</p>
-->
                    </div>
                </div>
            </div>
            
            <div class="main">

                <div id="first" class="main-item">
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                               <div class="article-box" style="margin-bottom: 3rem;">
                                   <h2 class="title">歷久不衰的夾娃娃機</h2>
                                    <h2 class="title">背後消費者的心理需求</h2>
                                </div>
                                <img class="img-fluid" src="static/images/gragrago/gragrago_1.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-item">
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                                <div class="article-box">
                                   <h2 class="title">台灣市場說明</h2>
                                    <div class="p-box">
                                        <p>2017年突然變成營收金雞母，</p>
                                        <p>2015年全台娃娃機店整年的總銷售額僅2億8千萬，</p>
                                        <p>2017整年度年銷售額已經突破10億大關，</p>
                                        <p>兩年內總體營收已成長近4倍。可見該產業成長速度之驚人，</p>
                                        <p>吸引眾多人紛紛投入，欲分食這塊娃娃大餅。</p>
                                    </div>
                                </div>
                                <img class="img-fluid" src="static/images/gragrago/gragrago_2.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
<?php /*
                <div class="main-item">
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                                <img class="img-fluid" src="static/images/gragrago/gragrago_3.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-item">
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                                <img class="img-fluid" src="static/images/gragrago/gragrago_4.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-item">
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                                <img class="img-fluid" src="static/images/gragrago/gragrago_5.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-item">
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                                <img class="img-fluid" src="static/images/gragrago/gragrago_6.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-item">
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                                <img class="img-fluid" src="static/images/gragrago/gragrago_7.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-item">
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                                <img class="img-fluid" src="static/images/gragrago/gragrago_8.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-item">
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                                <img class="img-fluid" src="static/images/gragrago/gragrago_9.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
*/?>
            </div>
        </div>
        <?php include_once ("includes/footer.php"); ?>
    </body>
</html>