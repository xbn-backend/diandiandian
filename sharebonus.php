<!DOCTYPE html>
<html>
    <head>
        <?php include_once ("includes/head.php");  ?> 
        <?php include_once ("includes/cssjs.php");  ?>
        <title>雪波拿</title>
    </head>
    <body>
        <?php include_once("includes/header.php"); ?>
        <div id="sharebonus">
            <div class="page-content d-flex align-items-stretch">
                <div class="headerbanner">
                    <div class="content animated fadeInUp">
                        <img class="logo" src="static/images/sharebonustext.png" alt="">
                        <h1>雪波拿紅利集點神器</h1>
                        <p>所有民生必需的食衣住行育樂全方位的生活盡在雪波拿</p>
                        <p>獲得的點數隨處、隨時、隨地可用，讓你換到更大的優惠</p>
                        <p>點數不只能自己收集，還可以賺別人的!</p>
                    </div>
                </div>
            </div>
            
            <div class="main">
                <div id="first" class="main-item">
                    <h2 class="title">雪波拿讓你獲益二大的特色</h2>
                    <div class="article-box">
                        <img class="img-fluid" src="static/images/about01.png" alt="">
                        <div class="p-box">
                            <p>雪波拿串聯了民生必需的食衣住行育樂等全方位的生活，將店家的點數整合後，讓消費者換到更大的優惠，獲得的點數隨處、隨時、隨地可用，讓你的點數不只有自己的，還可以賺別人的!</p>
                        </div>
                    </div>
                    
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md order-2 order-md-1">
                                <h3>消費得點數 你我一起賺</h3>
                                <div class="p-box">
                                    <p>致力能讓更多的人得到更好的生活品質，</p>
                                    <p>讓購物行為成為你賺取點數，甚至改善生活的一個平台</p>
                                    <p>購物後將獲得的點數分享給朋友，</p>
                                    <p>讓朋友加入雪波拿和你一起累積，</p>
                                    <p>你的分享，換來朋友一輩子的連結。</p>
                                    <p>朋友分享點數給你</p>
                                    <p>讓你更快獲得點數換贈品!</p>
                                </div>
                            </div>
                            <div class="col-md order-1 order-md-2">
                                <img class="img-fluid" src="static/images/about02.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                                <img class="img-fluid" src="static/images/about04.png" alt="">
                            </div>
                            <div class="col-md">
                                <h3>網路社群聯結 打造更多資源</h3>
                                <div class="p-box">
                                    <p>打造自己專屬的分享連結</p>
                                    <p>朋友因你而省錢</p>
                                    <p>你因分享而獲得點數</p>
                                    <p>透過網路行為</p>
                                    <p>增加你人脈 倍增更多機會！</p>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
                <div class="main-item">
                    <h2 class="title">如何花賺來的點數</h2>
                    <div class="article-box">
                        <img class="img-fluid" src="static/images/about2-01.png" alt="">
                        <div class="p-box">
                            <p>雪波拿串聯了民生食衣住行育樂等全方位的生活必需，</p>
                            <p>店家的點數經整合後，消費者能透過雪波拿App輕鬆享有更高的生活樂趣！</p>
                        </div>
                    </div>

<!--
                <div class="row article01">
                    <div class="col-md-6">
                        <h3>消費得積分 你我一起賺  (刪掉)</h3>
                        <p>
                            讓你更快獲得點數換贈品!
                        </p>
                    </div>
                    <div class="col-md-6"><img src="images/about02.png" alt=""></div>
                </div> 
-->
<!--
                    <div class="row article01">  
                        <div class="col-md-6"><img src="images/about04.png" alt=""></div>
                        <div class="col-md-6">
                            <h3>互聯網社群聯結 打造更多資源(刪掉) </h3>
                            <p>
                                踩著互聯網的風口賺到一筆小錢
                            </p>
                        </div>
                    </div>
-->
                </div>
                <div class="main-item">
                    <h2 class="title">如何快速賺取點數</h2>
                    <div class="article-box">
                        <img class="img-fluid" src="static/images/about3-01.png" alt="">
                    </div>
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md order-2 order-md-1">
                                <h3>朋友消費送你點數</h3>
                                    <div class="p-box">
                                        <p>使用專屬邀請碼</p>
                                        <p>邀請朋友加入雪波拿</p>
                                        <p>不只可在消費過程中累積點數</p>
                                        <p>還能得到朋友分享的額外點數</p>
                                        <p>朋友越多 點數越多！</p>
                                    </div>
                                </div>
                            <div class="col-md order-1 order-md-2">
                                <img class="img-fluid" src="static/images/about3-02.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="article-box">
                        <div class="row">
                            <div class="col-md">
                                <img class="img-fluid" src="static/images/about3-03.png" alt="">
                            </div>
                            <div class="col-md">
                                <h3>商家的客戶變成你的點數來源</h3>
                                <div class="p-box">
                                    <p>介紹商家加入雪波拿，商家能享有其他商家的消費者</p>
                                    <p>也能累積點數取得被動收入</p>
                                    <p>因商家而加入雪波拿的客人</p>
                                    <p>未來消費點數也會回饋給商家的推薦者</p>
                                    <p>讓大家一起來幫你收集點數！</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-last-item">
                    <img class="img-fluid" src="static/images/about3-04.png" alt="">
                </div>
            </div>
        </div>
        <?php include_once ("includes/footer.php"); ?>
    </body>
</html>